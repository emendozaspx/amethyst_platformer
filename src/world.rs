use amethyst::{
    prelude::*,
};

pub struct GamePlayState;

impl SimpleState for GamePlayState {
    fn on_start(&mut self, _data: StateData<'_, GameData<'_, '_>>) {}
}
