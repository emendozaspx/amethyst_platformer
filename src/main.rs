mod lib;
mod world;

use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{
        types::DefaultBackend,
        RenderingSystem,
    },
    utils::application_root_dir,
    window::WindowBundle,
};

use lib::RenderingGraph;
use world::GamePlayState;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let resources_dir = app_root.join("resources");
    let display_config_path = resources_dir.join("display_config.ron");

    let game_data = GameDataBuilder::default()
        .with_bundle(WindowBundle::from_config_path(display_config_path))?
        .with_bundle(TransformBundle::new())?
        .with_thread_local(RenderingSystem::<DefaultBackend, _>::new(
            RenderingGraph::default(),
        ));

    let mut game = Application::new(resources_dir, GamePlayState, game_data)?;
    game.run();

    Ok(())
}

